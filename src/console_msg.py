# WP Multiool
# Copyright (C) 2020-2023  [=World Players=> Valde & Guto 
# http://wpserver.rf.gd, https://codeberg.org/wpserver, adm.wp@groupmail.com

# This file is part of WP Multitool.

# WP Multitool is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# WP Multitool is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with WP Multitool.  If not, see <https://www.gnu.org/licenses/>.

__all__ = ["ConsoleMessage"]

from random import choice

from _base import Base
from mtfuncs import get_cfg
from mtconsts import GREEN, NO_COLOR
from mtregex import remsg


class ConsoleMessage(Base):
    def __init__(self, sockets) -> None:
        super().__init__(sockets)

        self.open_file("console_messages.dat")

        self.msgs = []

        self.time = get_cfg()["console_msg"]["time"]
        self.random = get_cfg()["console_msg"]["random"]
        self.__check_instances()

    def __check_instances(self):
        if isinstance(self.time, (float, int)):
            if self.time < 30.0:
                self.time = 30.0
        else:
            self.time = 180.0

        if not isinstance(self.random, bool):
            self.random = True

    def run(self) -> None:
        print(f'{self.__class__.__name__:.<35} {GREEN}running{NO_COLOR}')

        while not self.event.is_set():
            self.load_messages()

            if not self.msgs:
                self.event.wait(self.time)
                continue

            try:
                if self.random:
                    self.send_message(choice(self.msgs))
                else:
                    self.send_message(self.msgs[0])
                    self.msgs.append(self.msgs.pop(0))
            except Exception as error:
                print(f'{self.__class__.__name__} error: {error}.')
                break

            self.event.wait(self.time)

        self.file.close()

    def load_messages(self) -> None:
        temp_msgs = []

        for msg in self.file:
            msg = remsg.search(msg)
            if not msg:
                continue

            msg = msg.groups()[0]
            temp_msgs.append(msg)
            if msg in self.msgs:
                continue

            self.msgs.append(msg)

        self.msgs = [msg for msg in self.msgs if msg in temp_msgs]
        self.file.seek(0)

    def send_message(self, msg: str) -> None:
        for sock in self.sockets.values():
            sock.send(bytes(f'ÿÿÿÿrcon {self.rpswd} say {msg}', 'latin-1'))
