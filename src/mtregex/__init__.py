# WP Multiool
# Copyright (C) 2020-2023  [=World Players=> Valde & Guto 
# http://wpserver.rf.gd, https://codeberg.org/wpserver, adm.wp@groupmail.com

# This file is part of WP Multitool.

# WP Multitool is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# WP Multitool is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with WP Multitool.  If not, see <https://www.gnu.org/licenses/>.

__all__ = ["restatus", "resub", "remap", "reonline"]

import re

restatus = re.compile(
    r"^\s+(\d+)\s+(-?\d+)\s+(\d+)(?<=[0-9])\s+(.{1,31}?)\s+(\d+)\s+"
    r"(\d+\.\d+\.\d+\.\d+\:-?\d{1,5})\s*(\d+)\s+(\d+)$", re.M | re.I
    )
resub = re.compile(r"\^[0-7]|\W|_", re.I|re.M)
remap = re.compile(r"^\s*map:\s*(.+)$", re.I|re.M)
# remap = re.compile(r"mapname\\(.+?)\\", re.I|re.M)
reonline = re.compile(r'\d+\s*\d+\s*"(.+)"', re.I|re.M)
remsg = re.compile(r"^>\s?\"(.+)\"$", re.M | re.I)
