__default_data = {
    "sv_data": {
        "server_ip": "127.0.0.1",
        "server_ports": [28960],
        "rcon_password": "password_here"
    },

    "auto_map": {
        "enabled": 0,
        "amap_ports": [28960],
        "amap_minplayer": 4,
        "amap_norepeat": 1
    },

    "auto_renamer": {
        "enabled": 0,
        "time": 1.0,
        "prefix": "^2[=^3World Players^2=>"
    },

    "bad_name": {
        "enabled": 0,
        "time": 1.0
    },

    "console_msg": {
        "enabled": 0,
        "time": 180.0,
        "random": True
    },

    "mem_cleanup": {
        "enabled": 0,
        "days": 7
    }
}