# ==== Variables ==== #
FILE = "mtconfig.json"
OUT_SECS = 0.5

# ==== ANSI color ==== #
RED = "\033[0;31m"
GREEN = "\033[0;32m"
YELLOW = "\033[0;33m"
NO_COLOR = "\033[m"
