# WP Multiool
# Copyright (C) 2020-2023  [=World Players=> Valde & Guto 
# http://wpserver.rf.gd, https://codeberg.org/wpserver, adm.wp@groupmail.com

# This file is part of WP Multitool.

# WP Multitool is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# WP Multitool is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with WP Multitool.  If not, see <https://www.gnu.org/licenses/>.

__all__ = ['BadName']

from socket import socket

from _base import Base
from mtfuncs import get_cfg
from mtregex import restatus, resub
from mtconsts import GREEN, NO_COLOR


class BadName(Base):
    def __init__(self, sockets) -> None:
        super().__init__(sockets)

        self.open_file("bad_names.dat")

        self.bad_names = []

        self.time = get_cfg()["bad_name"]["time"]
        self.__check_instance()

    def __check_instance(self):
        if isinstance(self.time, (float, int)):
            if self.time < 0.5:
                self.time = 0.5
        else:
            self.time = 1.0

    def run(self) -> None:
        print(f'{self.__class__.__name__:.<35} {GREEN}running{NO_COLOR}')

        while not self.event.is_set():
            self.event.wait(self.time)

            self.bad_names = self.file.readlines()
            self.file.seek(0)

            if not self.bad_names:
                continue

            try:
                for sock in self.sockets.values():
                    self.check_names(sock)
            except Exception as error:
                print(f'{self.__class__.__name__} error: {error}.')
                break

        self.file.close()

    def check_names(self, sock: socket) -> None:
        def kick_player() -> None:
            sock.send(
                bytes(f'ÿÿÿÿrcon {self.rpswd} say ^7^1{name} ^7has been '
                      'kicked for ^1bad name^7.', 'latin-1')
                )

            self.event.wait(3.0)

            sock.send(
                bytes(f'ÿÿÿÿrcon {self.rpswd} ClientKick {idp}', 'latin-1')
                )


        status = restatus.findall(self.get_server_status(sock))

        for idp, _, ping, name, _, _, _, _ in status:
            if ping in ['CNCT', '999', 'ZMBI']:
                continue

            new_name = resub.sub("", name).lower()

            for bad_name in self.bad_names:
                if bad_name.startswith("#"):
                    if bad_name[1:].strip().lower() in new_name:
                        kick_player()
                        break
                # elif bad_name.startswith("$"):
                #     if new_name in bad_name.lower():
                #         kick_player()
                #         break
