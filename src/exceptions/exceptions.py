__all__ = ["BadRconPasswordError", "NoSocketError", "NoThreadError"]


class BadRconPasswordError(Exception):
    pass


class NoSocketError(Exception):
    pass


class NoThreadError(Exception):
    pass
