# WP Multiool
# Copyright (C) 2020-2023  [=World Players=> Valde & Guto 
# http://wpserver.rf.gd, https://codeberg.org/wpserver, adm.wp@groupmail.com

# This file is part of WP Multitool.

# WP Multitool is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# WP Multitool is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with WP Multitool.  If not, see <https://www.gnu.org/licenses/>.

__all__ = ['wp_title', 'get_cfg', 'clear', 'create_folders', 'create_jsonfile',
           'get_sockets']

import socket
import json
import os
from sys import platform

from exceptions import BadRconPasswordError, NoSocketError
from mtconsts import *
from mtconsts import __default_data


def wp_title(title: str, ver: str) -> None:
    clear()
    print(f'{GREEN}', '=' * 79, f'{NO_COLOR}', sep='')
    print(f'{title.center(100)}\n\n\n')
    print(f'Made by: Valde & Guto{ver:>58}')
    print(f'{YELLOW}', '=' * 79, f'{NO_COLOR}', sep='')


def get_cfg() -> dict:
    """Return data from JSON file"""
    with open(f'cfg/{FILE}', encoding='utf-8') as data:
        return json.load(data)


def clear() -> None:
    """Clear console/terminal"""
    if 'win' in platform.lower():
        os.system('cls')
    else:
        os.system('clear')


def create_jsonfile() -> None:
    if not os.path.exists("cfg"):
        os.mkdir("cfg")

    if FILE not in os.listdir('cfg'):
        with open(f'cfg/{FILE}', 'w', encoding='utf-8') as wfile:
            json.dump(__default_data, wfile, indent=4)

        clear()

        print(f'File "{FILE}" successful created in "cfg" folder.\n'
              'Configure your server in this file.')

        raise SystemExit


def get_sockets() -> dict:
    sockets = {}

    ip = get_cfg()["sv_data"]["server_ip"].strip()
    port_list = get_cfg()["sv_data"]["server_ports"]
    rpswd = get_cfg()["sv_data"]["rcon_password"].strip()

    for port in port_list:
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.settimeout(OUT_SECS)
        sock.connect((ip, port))

        sock.send(bytes(f"ÿÿÿÿrcon {rpswd} status", "latin-1"))
        try:
            data = sock.recv(1024).decode("latin-1")
            if "BAD RCONPASSWORD" in data.upper():
                sock.close()
                raise BadRconPasswordError("wrong rcon password")
        except socket.timeout:
            print(f"{RED}Unable to connect to the server {ip}:{port}{NO_COLOR}")
            sock.close()
        else:
            sockets[port] = sock

    if not sockets:
        raise NoSocketError("no socket connection")

    return sockets


if __name__ == "__main__":
    wp_title(f"{GREEN}[={YELLOW}World Players{GREEN}=> {NO_COLOR}Multitool Free",
             "Version: Test")
