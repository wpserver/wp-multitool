# WP Multiool
# Copyright (C) 2020-2023  [=World Players=> Valde & Guto 
# http://wpserver.rf.gd, https://codeberg.org/wpserver, adm.wp@groupmail.com

# This file is part of WP Multitool.

# WP Multitool is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# WP Multitool is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with WP Multitool.  If not, see <https://www.gnu.org/licenses/>.

__all__ = ["AutoMap"]

from socket import socket
from random import choice

from _base import Base
from mtfuncs import get_cfg
from mtregex import restatus, remap
from mtconsts import GREEN, NO_COLOR


class AutoMap(Base):
    def __init__(self, sockets) -> None:
        super().__init__(sockets)

        self.open_file("auto_maps.dat")

        self.map_list = []
        self.temp_map = []

        self.amap_ports = get_cfg()["auto_map"]["amap_ports"]
        self.min_player = get_cfg()["auto_map"]["amap_minplayer"]
        self.no_repeat = get_cfg()["auto_map"]["amap_norepeat"]
        self.__check_instances()

        del self.amap_ports

    def __check_instances(self) -> None:
        if isinstance(self.amap_ports, (list, tuple)):
            self.sockets = {
                k: v for k, v in self.sockets.items() if k in self.amap_ports
                }
            if not self.sockets:
                raise SystemExit
        else:
            raise SystemExit

        if isinstance(self.min_player, int):
            if self.min_player < 0:
                self.min_player = 0
        else:
            self.min_player = 4

        if not isinstance(self.no_repeat, int):
            self.no_repeat = 1

    def run(self) -> None:
        print(f'{self.__class__.__name__:.<35} {GREEN}running{NO_COLOR}')

        while not self.event.is_set():
            self.event.wait(120.0)

            self.load_maplist()

            if not self.map_list:
                continue

            try:
                for sock in self.sockets.values():
                    self.auto_map_changer(sock)
            except Exception as error:
                print(f'{self.__class__.__name__} error: {error}.')
                break

        self.file.close()

    def load_maplist(self) -> None:
        self.map_list = [
            map.strip().lower()[1:] for map in self.file
            if map.strip().startswith('#')
            ]

        self.file.seek(0)

    def auto_map_changer(self, sock: socket) -> None:
        nv_map = choice(self.map_list)
        if nv_map in self.temp_map:
            self.event.wait(0.1)
            return self.auto_map_changer()

        status = self.get_server_status(sock)

        sv_map = remap.findall(status)
        if sv_map:
            sv_map = sv_map[0].lower()
            online = len(restatus.findall(status))
        else:
            return None

        if sv_map in self.map_list or online > self.min_player:
            return None

        sock.send(
            bytes(f'ÿÿÿÿrcon {self.rpswd} ^3There\'s few players online. '
                  'A small map has been chosen automatically!', 'latin-1')
            )
        self.event.wait(7.0)
        sock.send(
            bytes(f'ÿÿÿÿrcon {self.rpswd} ^3It will be changed to ^2{nv_map} '
                  '^3in 7 sec...', 'latin-1')
            )
        self.event.wait(7.0)
        sock.send(bytes(f'ÿÿÿÿrcon {self.rpswd} map {nv_map}', 'latin-1'))
        self.event.wait(7.0)

        if self.no_repeat <= 0 or self.no_repeat >= len(self.map_list):
            return None

        self.temp_map.insert(0, nv_map)
        if len(self.temp_map) > self.no_repeat:
            self.temp_map.pop()
