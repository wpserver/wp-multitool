# WP Multiool
# Copyright (C) 2020-2023  [=World Players=> Valde & Guto 
# http://wpserver.rf.gd, https://codeberg.org/wpserver, adm.wp@groupmail.com

# This file is part of WP Multitool.

# WP Multitool is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# WP Multitool is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with WP Multitool.  If not, see <https://www.gnu.org/licenses/>.

__all__ = ["Base"]

import os
from typing import Dict
from socket import socket
from threading import Thread, Event
from abc import ABC, abstractmethod

from mtfuncs import get_cfg


class Base(Thread, ABC):
    def __init__(self, sockets: Dict[int, socket]) -> None:
        super().__init__()

        self.sockets: Dict[int, socket] = sockets
        self.rpswd = get_cfg()["sv_data"]["rcon_password"].strip()
        self.event = Event()

    @abstractmethod
    def run(self) -> None: pass

    def open_file(self, file: str = None) -> None:
        if not isinstance(file, str):
            return None

        if not os.path.exists("data"):
            os.mkdir("data")

        try:
            self.file = open(f'data/{file}', encoding='latin-1')
        except FileNotFoundError:
            open(f'data/{file}', 'x', encoding='latin-1')
            print(f'File "{file}" has been created in "data" folder!')
            return self.open_file(file)

    def get_server_status(self, sock: socket) -> str:
        sock.send(bytes(f'ÿÿÿÿrcon {self.rpswd} status', 'latin-1'))
        temp_data = ''
        status = ''
        while True:
            try:
                temp_data = sock.recv(8192).decode('latin-1')
                status += temp_data
            except:
                break

        return status

    def terminate(self) -> None:
        self.event.set()
