# WP Multiool
# Copyright (C) 2020-2023  [=World Players=> Valde & Guto 
# http://wpserver.rf.gd, https://codeberg.org/wpserver, adm.wp@groupmail.com

# This file is part of WP Multitool.

# WP Multitool is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# WP Multitool is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with WP Multitool.  If not, see <https://www.gnu.org/licenses/>.

__all__ = ['MemCleanup']

from subprocess import run as subproc_run

from _base import Base
from mtfuncs import get_cfg
from mtconsts import GREEN, NO_COLOR


class MemCleanup(Base):
    def __init__(self, sockets=None) -> None:
        super().__init__(sockets)

        self.time = 86400.0

        self.days = get_cfg()["mem_cleanup"]["days"]
        self.__check_instance()

        del self.days, self.sockets, self.rpswd

    def __check_instance(self):
        if isinstance(self.days, (float, int)):
            if self.days < 1:
                self.days = 0.5
        else:
            self.days = 7

        self.time *= self.days

    def run(self) -> None:
        print(f'{self.__class__.__name__:.<35} {GREEN}running{NO_COLOR}')

        while not self.event.is_set():
            self.event.wait(self.time)

            try:
                subproc_run(['sync'])
                subproc_run(['sysctl', '-q', '-w', 'vm.drop_caches=3'])
            except:
                print('Error: Do you need install "procps" on linux for use '
                      'Mem Cleanup.')
                break
