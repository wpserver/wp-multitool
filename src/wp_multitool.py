#! /usr/bin/env python3

# WP Multiool
# Copyright (C) 2020-2023  [=World Players=> Valde & Guto 
# http://wpserver.rf.gd, https://codeberg.org/wpserver, adm.wp@groupmail.com

# This file is part of WP Multitool.

# WP Multitool is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# WP Multitool is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with WP Multitool.  If not, see <https://www.gnu.org/licenses/>.

__author__ = "[=World Players=> Valde & Guto"
__version__ = "2.0.0-stable"

from time import sleep

from exceptions import BadRconPasswordError, NoSocketError, NoThreadError
from mtfuncs import wp_title, get_cfg, get_sockets, create_jsonfile
from mtconsts import FILE, GREEN, YELLOW, NO_COLOR


create_jsonfile()

threads = []
try:
    sockets = get_sockets()

    value = get_cfg()
    if value['auto_map']['enabled'] == 1:
        from auto_map import AutoMap
        threads.append(AutoMap(sockets))

    if value['auto_renamer']['enabled'] == 1:
        from auto_renamer import AutoRenamer
        threads.append(AutoRenamer(sockets))

    if value["bad_name"]['enabled'] == 1:
        from bad_name import BadName
        threads.append(BadName(sockets))

    if value["console_msg"]['enabled'] == 1:
        from console_msg import ConsoleMessage
        threads.append(ConsoleMessage(sockets))

    if value['mem_cleanup']['enabled'] == 1:
        from mem_cleanup import MemCleanup
        threads.append(MemCleanup())

    if not threads:
        raise NoThreadError

    del value, create_jsonfile
except KeyError:
    print(f'It\'s missing a key on your {FILE} file.\n'
          f'Delete your {FILE} file and run wp_multitool again\n'
          f'for create a new "{FILE}" file.')
    raise SystemExit
except BadRconPasswordError:
    print("Bad rconpassword! Multitool finished.")
    raise SystemExit
except NoSocketError:
    print("Unable to establish a connection! Multitool finished.")
    raise SystemExit
except NoThreadError:
    print("None tool it's been enabled! Multitool finished.")
    raise SystemExit
except Exception as erro:
    print(f'Error: {erro}.\nException: {erro.__class__.__name__}\n'
          f'Optionaly, delete the "{FILE}" file and try run again.')
    raise SystemExit


wp_title(f"{GREEN}[={YELLOW}World Players{GREEN}=> {NO_COLOR}Multitool Free",
         f"Version: {__version__}")

for thread in threads:
    thread.start()

print("\n")

while True:
    try:
        input("Press ENTER to finish the Multitool...\n")
        break
    except:
        continue

for thread in threads:
    thread.terminate()

sleep(0.25)

for sock in sockets.values():
    sock.close()
