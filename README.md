# WP MULTITOOL

The **WP Multitool** is a free software which provides extras functionalities to the Call of Duty 1 and many others useful functionalities.

## INSTALLATION

To use our multitool, it's very simple. First of all, you must transfer the **wp_multitool** file to your server anywhere you want (we recommend to put it in unique folder). After transferred the file, do **sudo chmod +x wp_multitool** and execute **wp_multitool** on terminal in this way: **./wp_multitool**. The **mtconfig.json** file will to be generated in ***cfg*** folder.

## CONFIGURATION

After executed **wp_multiool** and generate the **mtconfig.json**, you need to configure your server and the functionalities you wish to use. To do this, you need to open the **mtconfig.json** file with any text editor you want (like the nano for Linux).

#### sv_data
Here you will to put the basic data of your server.

**sever_ip:** you must to add the IP of your server within the quotes after the colon (:).  
**server_ports:** you must to add your server port inside the square brackets [28960]. By default we leave port 28960, but do you can change it. If you have more than one server in different ports, you must separate each port with a comma: [28960, 28961, 28962].  
**rcon_password:** simply type your rcon password inside the quotes after the colon (:).

#### auto_map
The **Auto Map** when enabled, will change automatically to an another map if the number of players on-line is equal or less than the number defined by you. You can add the maps you wish in the **auto_maps.dat** file inside the **data** folder. The names must ever starts with **#** and break a line to each new map name: **#mp_harbor**.

**enabled:** set 1 to enable this module.  
**amap_ports:** Here, do you will add the ports of the servers you wishes the **Auto Map** will works in.  
**amap_minplayer:** this defines the number of minimum players on-line to change the map. For example, if you set 5 and have less/equal than 5 players on-line, it will change to a map from **auto_maps.dat** if it not in **auto_maps.dat**. Minimum value: 0.  
**amap_norepeat:** defines when a map changed once time will can be choose again after the number of round defined. For example, if you set 2 and "mp_harbor" was chosen once time ago, the mp_harbor will to be available again only after 2 rounds.  

#### auto_renamer
The **Auto Renamer** when enabled, will change a forbidden name to an another defined by you. The forbidden names must be added in the **auto_renames.dat** file inside the **data** folder. The names must ever starts with **#** and break a line to each new name: **#Admin**. To long names which have space, it must be written without space: **#UnnamedPlayer**. This module needs your server have the **CoDaM** with the **HamGoodies** plug-in to it works.

**enabled:** set 1 to enable this module.  
**time:** Defines the time (in seconds) that each message will be sent (60 = 1 minute). Minimum value: 0.5.  
**prefix:** Defines to which name to be changed when a player is using a forbidden name.  

#### bad_name
The **Bad Name** when enabled, will to kick a player which is using a forbidden name. The forbidden names must be added in the **bad_names.dat** file inside the **data** folder. The names must ever starts with **#** and break a line to each new name: **#Admin**. To long names which have space, it must be written without space: **#UnnamedPlayer**.

**enabled:** set 1 to enable this module.  
**time:** Defines the time (in seconds) that each message will be sent (60 = 1 minute). Minimum value: 0.5.  

#### console_msg
The **Console MSG** when enabled, will to send messages to everyone in the server. The messages must be added in the **console_messages.dat** file inside the **data** folder. To each message line, you must starts with **>** and add the message inside **""**: **>"Message here."** or **> "Message here"**.

**enabled:** set 1 to enable this module.  
**time:** Defines the time (in seconds) that each message will be sent (60 = 1 minute). Minimum value: 30.  
**random:** Use **true** to activate the sending of messages in random way. Use **false** to the messages be sent in order way (first to last).  

#### mem_cleanup
The **Mem Cleanup** when enabled, will to clean the memory of your server. It's pretty useful to servers with few memory. It needs to have the **procps** installed in your Linux to it works.

**enabled:** set 1 to enable this module.  
**days:** refers to the period that the **mem cleanup** will to be done. If defined to 0.5, so it means it will to be executed each 12 hours.  

## Notes

If you is you using the old version of the **WP Multitool** and you want to update to this new version, you must to delete the old **JSON** file and reconfigure your server, cuz the new version works totally different than the old version.

The files to the **data** folder will to be create automatically when you run the software and you could edit these files even if the software is running. The names of the files **.dat** has been totally modified. You can rename your old files names to the new names; however, you must do this only when the software is not running.

If you do not enter the value corresponding to the data type (integer, string, floating and boolean) in the **JSON** file, then pre-defined data will be used (default value).

This software was tested only in **CoDExtended** old version, if you face bugs when use it, try to downgrade your **CoDExtended**.

## REQUIREMENTS

The binary version of this software was tested only on Ubuntu 16.04 (and Debian version equivalent) and above. Optionally you can try to run this software directly in another Linux distro, using their source code.

## LICENSE

This software is under the **GPLv3** license (https://www.gnu.org/licenses/gpl-3.0.html). You can find the source code, at: https://codeberg.org/wpserver.

## BUGS

You can report bugs to **Valde#3584** (Discord).
